import React, { Component } from 'react';
import AddTaskForm from './components/AddTaskForm/AddTaskForm_component';
import Task from './components/Task/Task_component';
import './App.css';

class App extends Component {

  state = {
    texts : [
        {text: 'Do homework', isFinished: false},
        {text: 'Buy bread', isFinished: false},
        {text: 'By milk', isFinished: false},
    ],
      text: '',
  };


    addTasks = () => {
      
      const copyArray = [...this.state.texts];

      const newTask = {text: this.state.text};

      copyArray.push(newTask);

      this.setState({texts: copyArray})

    };

    changeHandler = event => {
        this.setState({
            text: event.target.value
        })
    };

    deleteTask = (index) => {
      const copyTexts = [...this.state.texts];

      copyTexts.splice(index, 1);

      this.setState({texts: copyTexts});

    };

    done = (index, value) => {

      let tasks = this.state.texts;
      let task = this.state.texts[index];
      task.isFinished = value;
      tasks[index] = task;

      this.setState({texts: tasks})

    };

  render() {
    return (
      <div className="App">

          <AddTaskForm onChange={(event) => this.changeHandler(event)}
                       addTasks={() => this.addTasks()}
          />

          <Task state={this.state}
                deleteTask={this.deleteTask}
                done={this.done}
          />

      </div>
    );
  }
}

export default App;