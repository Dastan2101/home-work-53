import React from 'react';
import './addTask.css';

const AddTask = props => {
    return (
        <div className="main-block">

        <input type="text" placeholder="Add new task" value={props.text} onChange={props.onChange} id="input"/>
        <button onClick={props.addTasks} id="btn">Add</button>

        </div>
    );
}

export default AddTask;















