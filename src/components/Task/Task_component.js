import React from 'react';
import './block-task.css';

const Task = props => {
    return (
        props.state.texts.map((text, index) => {
            return (
                <div className="block-task" key={index}>
                    <p className="text">
                        {text.text}
                    </p>
                    <button onClick={() => props.deleteTask(index)} className="delete-btn">Delete</button>
                    <input type="checkbox" onChange={(e) => props.done(index, e.target.checked)} className="checkbox"/>

                    {text.isFinished ? <p>Done!</p> : null}
                </div>
            )
        })
    )
};

export default Task;
